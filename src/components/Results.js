import React from 'react';
import { Image, Row, Col } from 'antd';

const Results = ({ results, points }) => {
    /*
        Scales the points to percentage and finds the correct result to display
    */
    const scaledPoints = points * 100 / 20;
    const answer = results.find((result) => (scaledPoints >= result.minpoints) && (scaledPoints<=result.maxpoints))

    return (
        <div>
            <Row align="middle" style={{ marginTop: '20px', marginBottom: '20px' }}> 
                <Col span={12}>
                    <h2>{answer.title}</h2>
                    <p>{answer.message}</p>
                </Col>
                <Col span={12}>
                    <h2 style={{textAlign: 'right'}}>{`${scaledPoints}%`}</h2>
                </Col>
                </Row>
            <Image style={{ margin: '0 auto', display: 'block', paddingBottom: '20px' }} width={300} src={answer.img} />
        </div>
    )
}

export default Results;