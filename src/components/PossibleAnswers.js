import React from 'react';
import Checkbox from '../components/Checkbox';
import { Radio } from 'antd';


const PossibleAnswers = ({ question, onRadioSelect, possibleAnswers, submit, onCheckboxSet }) => {
    /*
        The id of each answer is passed and there are 4 cases to determine which color to return. 
            1.  If the user selected the answer and the answer is correct, then a green color is returned.
            2.  If the user selected the answer and the answer is not correct, then a red color is returned.
            3.  If the user has not selected the answer and the answer is correct and the possible answers array has at least an item, then a green color is returned.
            4.  If the user neither selected the answer nor the answer is correct, then the background color does not change.
    */
    const changeColor = (id) => {
        if (possibleAnswers.includes(id) && isAnswerCorrect(question,id)) 
            return 'correctAnswer'
         else if (possibleAnswers.includes(id) && !isAnswerCorrect(question,id)) 
            return 'wrongAnswer';
        else if (!possibleAnswers.includes(id) && isAnswerCorrect(question,id) && possibleAnswers.length > 0) 
            return 'correctAnswer';
        else if (!possibleAnswers.includes(id) && !isAnswerCorrect(question,id)) 
            return '';
    }

    /*
        Checks if the question is an array (that means that is a multiple answer question) and checks if 
        the id is includes in the correct_answer array. If is a single answer question the value of id is
        checked against the correct_answer value.
    */
   const isAnswerCorrect = (question, id) => {
        if (Array.isArray(question.correct_answer))
            return question.correct_answer.includes(id)
        else
            return question.correct_answer == id
   }
    
            switch (question.question_type) {
                case 'mutiplechoice-single': {
                    return (<Radio.Group  className={"input-wrapper" } onChange={onRadioSelect} value={possibleAnswers[0]}>
                                {question.possible_answers.map((answer) => (<Radio disabled={submit} className={'question-input '+(submit ? changeColor(answer.a_id) : '') } key={answer.a_id} value={answer.a_id}>{answer.caption}</Radio>))};
                            </Radio.Group>)
                }
                case 'mutiplechoice-multiple': {
                    return (<div className={"input-wrapper"}>
                        {question.possible_answers.map((answer) => (
                            <Checkbox
                                submitPressed={submit}
                                changeColor={changeColor}
                                key={answer.a_id}
                                value={answer.a_id}
                                name={answer.caption}
                                onChange={onCheckboxSet} />
                        ))}
                    </div>
                    )
                }
                case 'truefalse': {
                    return (<Radio.Group className={"input-wrapper"} onChange={onRadioSelect} value={possibleAnswers[0]}>
                        <Radio disabled={submit} className={'question-input ' + (submit ? changeColor(0) : '')} key={'false'} value={0}>False</Radio>
                        <Radio className={'question-input '+(submit ? changeColor(1) : '') } key={'true'} value={1}>True</Radio>
                    </Radio.Group>)
                }
            }
        
    }

export default PossibleAnswers;