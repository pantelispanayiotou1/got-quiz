import React from 'react';
import '../App.css';

const Checkbox = ({ type = 'checkbox', value, name, onChange, submitPressed, changeColor }) => (
    <div>
    <label className={'question-input ' + (submitPressed ? changeColor(value) : null)}>
        <input disabled={submitPressed} type={type} value={value} onChange={onChange} /> {'  '}
        {name}
        </label>
        </div>
 
);

export default Checkbox;