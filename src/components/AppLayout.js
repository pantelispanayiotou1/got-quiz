import React from 'react';
import { Layout, Menu } from 'antd';
import '../App.css';
const { Header, Content } = Layout;


const AppLayout = (props) => {
    const { children, title } = props;
   
  return ( <Layout className="layout">
      <Header className={'headerWrapper'}>
          <h2 className={'headerStyle'}>{title}</h2>
        </Header>
        <Content style={{ padding: '0 50px' }}>
            
            <div className="site-layout-content">
                {children}
            </div>
        </Content>
      
  </Layout>
  )
}
export default AppLayout;