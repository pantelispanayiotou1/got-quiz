import React, {Component} from 'react';
import { Image, Row, Col } from 'antd';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import PossibleAnswers from '../components/PossibleAnswers';
import wrong_answer from '../assets/wrong_answer.mp3';
import correct_answer from '../assets/correct_answer.mp3';
import '../App.css';

class Question extends Component {

    constructor(props) {
        super(props);
        this.state = {
            possibleAnswers: [], 
            submit: false, 

        }
    }
    /*
        When the question accepts only one answer, the possibleAnswers array value is checked
        against question's possible answer and returns if the answer is correct or not
    */
    validateSingleAnswer = () => {
        const { question } = this.props;
        let answer = question.correct_answer == this.state.possibleAnswers[0] ? true : false;
        if (answer)
            this.props.updatePoints(question.points);
        return answer;
    }
    /*
        When the question accepts multiple answers, the answers given from the user are sorted (if the user selected the answers in different order)
        and checked against question's possible answers. The function returns a flag if the answer given is correct or not.
    */
    validateMultiAnswer = () => {
        const { question } = this.props;
        const answers = this.state.possibleAnswers.sort();
        let flag = true;
        if (answers.length != question.correct_answer.length) {
            flag = false; 
        }  
        else {
            for (var i = 0; i < answers.length; i++) {
                if (question.correct_answer[i] != answers[i]) {
                    flag = false;
                    break;
                }    
            }
        }
        
        if (flag)
            this.props.updatePoints(question.points);
        
        return flag;
    }
    /*
        Receives a boolean value if the answer is correct or not, loads and plays the 
        respective sound.
    */
    playBeep = (answer) => {
  
         const sound = new Audio(answer ? correct_answer : wrong_answer).play();
         if (sound !== undefined) {
            sound
              .then(_ => {
                // autoplay started
              })
              .catch(err => {
                // catch dom exception
                console.info(err)
              })
          }
    }
   /*
        When the user submits the answers, the answers are validated and the correct and wrong 
        answers are highlighted. The results are shown for 3 seconds.
   */
    submit = () => {
        const { question } = this.props;
        this.setState({ submit: true }, () => {
            let answer = question.question_type === 'mutiplechoice-multiple' ? this.validateMultiAnswer() : this.validateSingleAnswer();
            this.playBeep(answer);
            setTimeout(() => {
                this.setState({ submit: false, possibleAnswers: [] });
                this.props.nextQuestion();
        },3000);
        })
        
    }

    /*
        The possibleAnswers state is updated when the user selects a radio answer.
    */
    onRadioSelect = (event) => {
        let possibleAnswers = [];
        possibleAnswers.push(event.target.value);
        this.setState({
            possibleAnswers
        });
    }
    /*
        When the user checks or unchecks an answer, the possibleAnswers array is updated with the 
        answers of the user. It determines whether the answer is already checked or not and based on 
        the checked status, it removes or adds the selected value. 
    */
    onCheckboxSet = (event) => {
        let checked = event.target.checked;
        let value = parseInt(event.target.value);
        let { possibleAnswers } = this.state;
        
        if (checked) {
            this.setState({ possibleAnswers: possibleAnswers.concat(value) });
        }
        else {
            this.setState({ possibleAnswers: possibleAnswers.filter((answer) => answer != value) });
        }
    }


    render() {
        const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
        const { question } = this.props;
        const { submit } = this.state;
        return (
            
            question ?
                <div>
                    <Row align="middle" style={{ margin: '20px' }}> 
                        <Col span={22}>
                            <h3 >{question.title}</h3>
                        </Col>
                        <Col span={2}>
                            <span>{question.q_id}/8</span>
                        </Col>
                        
                    </Row>
                    <Image style={{ margin: '0 auto', display: 'block' }} width={300} src={question.img} />
                    <PossibleAnswers
                        question={question}
                        onRadioSelect={this.onRadioSelect}
                        possibleAnswers={this.state.possibleAnswers}
                        submit={submit}
                        onCheckboxSet={this.onCheckboxSet}
                        />
                    <Row align="middle" style={{ marginTop: '20px', marginBottom: '20px' }}> 
                        <Col span={20}>
                            <input disabled={this.state.possibleAnswers.length < 1 || submit} onClick={() => this.submit()} value={'Submit'} type="button" />
                        </Col>
                        <Col span={2}>
                            {submit ?
                                <Spin indicator={antIcon} />
                                : null
                            }
                        </Col>
                    </Row>
                       
                </div>
            : null
            
          
        )
    }
}

export default Question;