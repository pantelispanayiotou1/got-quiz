import React from 'react';
import '../App.css'
import { Divider } from 'antd';

const SubHeader = ({title,description}) => {
    return (
        <div className={'header'}> 
            <p>{title}</p>
            <Divider className={'divider'} plain>
                {description}
             </Divider>
        </div>
    )
}
export default SubHeader;