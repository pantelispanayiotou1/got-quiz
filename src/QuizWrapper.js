import React, { Component } from 'react';
import Question from './components/Question';
import AppLayout from './components/AppLayout';
import Results from './components/Results';
import { Spin } from 'antd';
import './App.css';
import SubHeader from './components/SubHeader';

class QuizWrapper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questions: null, 
            title: '', 
            description: '', 
            currentIndex: 0, 
            points: 0, 
            results: null
        }
    }
    componentDidMount() {
        this.fetchQuestions();
        this.fetchResult();
    }
    /*
        Function that is sent to the question component and 
        returns with the points gained on each question and updates the state
    */
    updatePoints = (points) => {
        this.setState({points: this.state.points + points})
    }

    /*
        Fetch of results json from the url given.
    */
    fetchResult = () => {
        fetch("https://proto.io/en/jobs/candidate-questions/result.json")
            .then((res) => res.json())
            .then((res) => {
                this.setState({results: res.results})
            })
            .catch((err) => {
                console.log(err);
            })
    }

    /*
        Fetch of questions json from the url given.
    */
    fetchQuestions() {
        this.setState({ loading: true }, () => {
            fetch("https://proto.io/en/jobs/candidate-questions/quiz.json")
                .then((res) => res.json())
                .then((result) => {
                    this.setState({ questions: result.questions, title: result.title, description: result.description, });
                })
                .catch((err) => {
                    console.log(err);
                })
        });
    }
    /*
        Function that is passed to the question component to 
        continue to the next question after updating the state
    */
    nextQuestion = () => {
        this.setState({currentIndex: this.state.currentIndex + 1})
    }
    render() {
        const { currentIndex, questions, description, title } = this.state; 
        return (
            <>
                {!questions ?
                    <div className={'spinner'}>
                    <Spin size={'large'} />
                    </div>
                    :
                    <div>
                        
                        <AppLayout title={title}>
                            <SubHeader description={description} />
                             {questions && (currentIndex <questions.length) ? 
                                <Question
                                    nextQuestion={this.nextQuestion}
                                    question={questions[currentIndex]}
                                    updatePoints={this.updatePoints}
                                />
                                :
                                <Results
                                    results={this.state.results}
                                    points={this.state.points}
                                />}
                            
                        </AppLayout>
                        
                    </div>
                }
            </>
              
        )
    }
    
}

export default QuizWrapper;