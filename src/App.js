import React from 'react';
import './App.css';
import QuizWrapper from './QuizWrapper';
import './App.css'

function App() {
  return (
    <div className="main">
        <QuizWrapper />
    </div>
  );
}

export default App;
