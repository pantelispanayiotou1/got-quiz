This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## How to run 

1.  git clone https://pantelispanayiotou1@bitbucket.org/pantelispanayiotou1/got-quiz.git
2.  cd got-quiz
3.  npm install
4.  npm start